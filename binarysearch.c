#include <stdio.h>
 int main()
{
   int i,beg,end,mid,n,search,array[100];
 
   printf("Enter number of elements\n");
   scanf("%d",&n);
 
   printf("Enter %d integers\n", n);
   for (i = 0; i < n; i++)
      scanf("%d",&array[i]);
 
   printf("Enter value to find\n");
   scanf("%d", &search);
 
   beg = 0;
   end = n - 1;
   mid = (beg+end)/2;
 
   while (beg <= end) 
   {
      if (array[mid] < search)
         beg = mid + 1;    
      else if (array[mid] == search) 
      {
         printf("%d found at location %d.\n", search, mid+1);
         break;
      }
      else
         end = mid - 1;
 
      mid = (beg + end)/2;
   }
   if (beg > end)
      printf("%d isn't present in the list.\n", search);
return 0;  
}