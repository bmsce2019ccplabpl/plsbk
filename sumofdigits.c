#include <stdio.h>
int main()
{
int n, a, sum = 0, rem;
printf("Enter a number\n");
scanf("%d", &n);
a = n;
while (a != 0)
{
rem = a % 10;
sum = sum + rem;
a = a/10;
}
printf("Sum of digits of %d = %d\n", n, sum);
return 0;
}