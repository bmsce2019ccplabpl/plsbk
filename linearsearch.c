#include <stdio.h>
int main()
{
  int a[5], pos, c, n;
  printf("Enter number of elements in array\n");
  scanf("%d", &n);
  printf("Enter %d numbers\n", n);
  for (c = 0; c < n; c++)
  scanf("%d", &a[c]);
  printf("Enter a number to search\n");
  scanf("%d", &pos);
  for (c = 0; c < n; c++)
  {
    if (a[c] == pos) 
    {
      printf("%d is present at position %d\n", pos, c+1);
      break;
    }
  }
  if (c == n)
  printf("%d is not present in the array\n", pos);
  return 0;
}